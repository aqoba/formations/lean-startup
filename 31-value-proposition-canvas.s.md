---
author: Thomas Clavier
title: Value Proposition Canvas
---

#

| | |
|-|-|
| Thomas Clavier | ![](../includes/thomas-clavier.jpg){height=160px} |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) |
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>

# Le cadre

![](../includes/cadre.svg)

# Depuis la dernière fois

* Qu'avez-vous fait ?
* Qu'avez-vous appris ?

# Value Proposition Canvas

![](../includes/3-steps-step-1.2.svg)

# 

**Proposition de valeur** : Description des bénéfices que les clients peuvent attendre de vos produits et services.

# Comment procéder ? {data-background-image="../includes/heart.jpg" data-state="white70"}

* **Créer de la valeur** : Ensemble des bénéfices de la proposition de valeur que vous créez pour attirer les clients
* **Observer les clients** : Ensemble des caractéristiques des clients que vous présumez, observez et vérifiez sur le marché.

# Value Proposition Canvas

* Le Profil des Clients qui permet de découvrir et formaliser votre connaissance du client
* La Carte de la valeur où vous décrivez comment vous envisagez de créer de la valeur pour ce client

# Value Proposition Canvas

* Lorsque que les deux convergent, il y a adéquation entre le problème du client et votre solution.
* Il permet en plus de formuler la proposition de valeur

# 

![](../includes/value-proposition-design-fr-annote.svg)

# Profils des clients

![](../includes/value-proposition-design-fr-clients.svg)

# Aspirations ![](../includes/vpd-aspirations.svg){height=1em}

Les aspirations des clients décrivent ce que les clients essayent d’accomplir dans leur domaine professionnel et dans leur vie.
Mettez-vous toujours à la place des clients !

# Aspirations ![](../includes/vpd-aspirations.svg){height=1em}

* **Aspirations fonctionnelles** Les tâches qu’ils essayent d’accomplir ou les problèmes qu’ils essayent de résoudre : Manger sainement, ne pas tondre la pelouse, classer leurs photos facilement
* **Aspirations sociales** Lorsqu’ils veulent avoir une bonne image d’eux- même, acquérir un statut, du pouvoir et être perçue par autrui : apparaître comme consommateur branché.
* **Aspirations personnelles/émotionnelles** Lorsque les clients cherchent un état émotionnel spécifique comme être en sécurité, se sentir bien : Passer des vacances sereines, passer un bon moment en famille.

# Trois rôles ![](../includes/vpd-aspirations.svg){height=1em}

* **Acheteur de valeur** Ce sont les aspirations liées à l’expérience d’achat : Comparer des offres, décider quels Produits acheter, se faire livrer, ...
* **Co-créateur de valeur** Ce sont les aspirations liées à sa contribution à la création de valeur : évaluer un service, commenter un produit, participer à la conception, ...
* **Passeur de valeur** Ce sont les aspirations liés à la fin du cycle de vie de la proposition de valeur : se désabonner, recycler le produit, le revendre,...

# Exemple ![](../includes/vpd-aspirations.svg){height=1em}

![](../includes/vpd-exemple-1.png){height=500px}

# Problèmes ![](../includes/vpd-problemes.svg){height=1em}

Les problèmes décrivent ce qui ennuie vos client avant, pendant et après l’accomplissement d’une aspiration ou ce qui les empêche de la réaliser. On y inclut aussi les risques.

# Problèmes ![](../includes/vpd-problemes.svg){height=1em}

* **Résultats et problèmes et caractéristiques non souhaités** Ces problèmes sont fonctionnels, sociaux, émotionnels, auxiliaires ou liés à une caractéristiques sont souhaités : Ca marche pas, j’ai l’air d’un plouc, je ne le sens pas, je n’aime pas la couleur, ...
* **Obstacles** C’est ce qui empêche les clients de réaliser une aspiration : Je n’ai pas le temps, je n’ai pas les moyens, ...
* **Risques** Ce qui pourrait aller de travers ou avoir des conséquences négatives importantes : Une faille de sécurité serait dangereuse pour nous, on n’arrive pas à joindre les clients, ...

# Exemple ![](../includes/vpd-problemes.svg){height=1em}

![](../includes/vpd-exemple-2.png){height=500px}

# Bénéfices clients ![](../includes/vpd-benefices.svg){height=1em}

Les bénéfices sont les résultats et les avantages que veulent les clients. Ils regroupent l’utilité fonctionnelle, les bénéfices sociaux, les émotions positives et les économies de coûts.

# Bénéfices clients ![](../includes/vpd-benefices.svg){height=1em}

* **Bénéfices requis** Sans eux, la solution ne répond pas à une aspiration: Un smartphone doit permettre de téléphoner
* **Bénéfices attendus** Ils ne sont pas indispensables au fonctionnement mais ils font partie du minimum attendu.  Un iphone doit être beau.
* **Bénéfices désirés** Les clients aimeraient pouvoir en disposer : Mon smartphone est compatible avec mes autres appareils.
* **Bénéfices inattendus** Ils vont au delà des attentes et des désirs des clients : Les filtres snapchat en réalité augmentée.

# Exemple ![](../includes/vpd-benefices.svg){height=1em}

![](../includes/vpd-exemple-3.png){height=500px}

# La carte de la valeur

![](../includes/value-proposition-design-fr-valeur.svg)

# Produits & Services ![](../includes/vpd-produits-et-services.svg){height=1em}

Il s’agit simplement de la liste de ce que vous proposez.

* **Matériels / tangibles** ex: Produits manufacturés, légumes, livres, ...
* **Immatériels** Droits d’auteur, des services comme l’assistance après-vente, conseil, ...
* **Numériques** Téléchargement de musique, jeux, ...
* **Financiers** Assurances, services de paiement, ...

Vous pouvez y lister les fonctionnalités, le détail des services ou les caractéristiques des produits

# Exemple ![](../includes/vpd-produits-et-services.svg){height=1em}

![](../includes/vpd-exemple-4.png){height=500px}

# Les solutions ![](../includes/vpd-solutions.svg){height=1em}

les solutions décrivent comment vos produits et vos services contribuent à soulager tel ou tel problème du client.

Comment vous envisager de supprimer ou d’atténuer
certains sujets qui ennuient vos clients avant,
pendant ou après avoir essayé de réaliser une
aspiration.

Comment vous contrez les éléments qui empêchent
de réaliser leur aspiration.

Les grandes propositions de valeur se concentrent
sur des problèmes importants pour les clients et les
traitent efficacement.

# Exemple ![](../includes/vpd-solutions.svg){height=1em}

![](../includes/vpd-exemple-5.png){height=500px}

# Créateurs de bénéfices ![](../includes/vpd-createurs-de-benefices.svg){height=1em}

Comment vos produits et vos services créent des
bénéfices pour les clients.

Comment vous envisager de produire des résultats
et des bénéfices que vos clients attendent,
souhaitent ou qui les surprendraient par de l’utilité
fonctionnelle, des bénéfices sociaux, des émotions
positives ou des économies de coûts.

Les grandes propositions de valeur se concentrent
sur des créateurs de bénéfices importants pour les
clients et auxqeuls que vos produits ou services
peut apporter quelque chose.

# Exemple ![](../includes/vpd-createurs-de-benefices.svg){height=1em}

![](../includes/vpd-exemple-6.png){height=500px}

# Challenger votre proposition de valeur {.small}

* Quelles économies rendraient vos clients heureux ? Quelles économies en termes de temps, d'argent et d'effort auraient de la valeur pour eux ?
* À quels niveaux de qualité s'attendent-ils et que souhaiteraient-ils plus ou moins ?
* Comment les propositions de valeur actuelles ravissent-elles vos clients ? Quelles caractéristiques spécifiques apprécient-ils ? Quelles performances et quelle qualité attendent-ils ?
* Qu'est-ce qui faciliterait la tâche ou la vie de vos clients ? Pourrait-il y avoir une courbe d'apprentissage plus plate, plus de services ou des coûts plus bas ?
* Quelles conséquences sociales positives vos clients souhaitent-ils ? Qu'est-ce qui les rend beaux ? Qu'est-ce qui augmente leur pouvoir ou leur statut ?
* Qu'est-ce que les clients recherchent le plus ? Cherchent-ils un bon design, des garanties, des fonctionnalités spécifiques ou plus ?
* De quoi rêvent les clients ? À quoi aspirent-ils ou qu'est-ce qui les soulagerait ?
* Comment vos clients mesurent-ils le succès et l'échec ? Comment évaluent-ils la performance ou le coût ?
* Qu'est-ce qui augmenterait les chances de vos clients d'adopter votre proposition de valeur ? Souhaitent-ils un coût moindre, un investissement moindre, un risque moindre ou une meilleure qualité ?

# Formaliser rapidement sa proposition de valeur

#

![](../includes/proposition-de-valeur.svg)

# Exemple

Notre **Livre** aide **les entrepreneurs** qui veu(len)t **créer une activité** en **évitant de perdre du temps avec des idées qui ne marchent pas** et **en créant des produits et des services qui intéressent les gens**

# Essayer plusieurs proposition de valeur et testez les.

# L’adéquation

![](../includes/adequation.svg){height=500px}

# Pour la prochaine fois

* Construire votre canvas de proposition de valeur et tester les hypothèses.

# 

:::: {.columns}
::: {.column width="30%"}
:::
::: {.column width="70%"}

![](../includes/logo-aqoba.svg){height=100px}


| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) | 
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |
:::
::::


<small>
Support de formation créé par Efidev, [![](../includes/logo-azae.svg){class=logo-azae-inline}](https://azae.net) et [![](../includes/logo-aqoba.svg){class=logo-aqoba-inline}](https://aqoba.net) sous licence [![](../includes/cc-by-sa.svg){class=cc-by-sa-inline}](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)
</small>

