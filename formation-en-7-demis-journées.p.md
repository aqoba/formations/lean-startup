---
title: Lean Startup en 7 ateliers
---

Une formation au Lean Startup en 7 demis journées

# 1. Introduction au Lean Startup, organiser et rythmer son travail

## Objectifs pédagogiques
* Appréhender par l’expérience les notions clé du Lean startup.
* Comprendre les tenants et les aboutissants de la méthode
* Se faire un avis critique sur la méthode
* Comprendre l'intérêt des ateliers suivants et pourquoi ils se déroulent dans cet ordre.
* Acquérir un outil clé pour organiser et rythmer son travail : Le Lean Startup Board

## Déroulé
* Atelier de mise en situation “Lean startup snowflake”
* Présentation académique de toute la méthode
* Atelier ancrage des apprentissages et débats sur les clés de réussite de la méthode Lean Startup
* Séquence Lean Startup Board : Présentation académique

# 2. Tout savoir sur le Lean Canvas

## Objectifs pédagogiques
* Business modèle vs Business plan
* Documenter sa vision (remplir les Leans Canvas en moins de 20 min)
* Construire un deck de slides pour pitcher son projet dans toutes les situations
* Organiser et rythmer son travail

## Déroulé
* Séquence Lean Canvas, en petits groupes de 2 ou 3 personnes alternance théorie et pratique toutes les 20 min
* Séquence Pitch :
    * en grand groupe on essaye de construire la théorie  : comment mécaniquement extraire ses slides de ses leans canvas
    * en petits groupe, on transforme les leans canvas en présentation
    * présentations croisés

## Des clés pour commencer à répondre aux questions
* À quels problèmes, difficultés, aspirations allez-vous répondre ?
* Quelle pourrait être la solution la plus adéquate ?
* Quel est votre marché ?
* Quels sont vos segments de clientèle ?
* Qui seront vos early adopters ?
* Quelles sont les barrières à l'entrée ?
* Comment m’organiser pour construire ma startup ?

# 3. Choisir le bon lean canvas et tester le problème et la cible.

## Objectifs pédagogiques
* Sélectionner de façon méthodique le prochain lean canvas à tester
* Dérisquer son projet
* Construire et mener des entretiens problèmes
* Construire et mener des entretiens cible

## Déroulé
* Séquence sélection et dérisquage de projet, en grand groupe sur un des projets des incubés présents en alternant théorie et pratique toutes les 20 min.
* Séquence entretiens problèmes, en petits groupes de 2 ou 3, alternance de théorie, de pratique et d’entraînement devant le groupe complet.
* Séquence entretiens cible, en petits groupes de 2 ou 3, alternance de théorie, de pratique et d’entraînement devant le groupe au complet.

## Des clés pour commencer à répondre aux questions
* À quels problèmes, difficultés, aspirations allez-vous répondre ?
* Quelle pourrait être la solution la plus adéquate ?
* Quelles sont les barrières à l'entrée ?
* Quel est votre marché ?
* Qui seront vos early adopters ?
* Quels sont vos segments de clientèle ?

# 4. Utiliser le value proposition canvas et le Lean Canvas pour tester la solution.

## Objectifs pédagogiques
* À l’aide du Value proposition canvas lui même construit depuis le Lean Canvas
* Comprendre les besoins et les attentes de son client, construire les personas associés
* Construire la solution minimal la plus adaptée
* Construire et mener des entretiens solutions
* Valider le périmètre de la solution et son prix.

## Déroulé
* Séquence Value proposition canvas, en petits groupes de 2 ou 3, alternance de théorie et de pratique.
* Séquence entretiens solutions, en petits groupes de 2 ou 3, alternance de théorie, de pratique et d’entraînement devant le groupe complet.

## Des clés pour commencer à répondre aux questions
* Quelle pourrait être la solution la plus adéquate ?
* Quelles sont les barrières à l'entrée ?

# 5. Définir et piloter la réalisation de son MVP et tester l’adéquation produit / marché

## Objectifs pédagogiques
* Définir le périmètre de son MVP à l’aide de la matrice de Kano
* Piloter et rythmer la réalisation de son MVP
* Construire et mener des entretiens MVP
* Comment manipuler le paper prototyping

## Déroulé
* Séquence “stories mapping”, en petits groupes de 4 ou 5, alternance de théorie, de pratique et d’échange avec le groupe complet. 
* Séquence matrice de Kano, en grand groupe, alternance de théorie et de pratique sur un cas fictif avant de passer en petits groupes sur les projets des incubés.
* Séquence entretiens MVP, en petits groupes de 2 ou 3, alternance de théorie, de pratique et d’entraînement devant le groupe au complet.
* Séquence paper prototyping, en petits groupes de 2 ou 3, alternance de théorie, de pratique et d’entraînement devant le groupe au complet.

## Des clés pour commencer à répondre aux questions

* À quels problèmes, difficultés, aspirations allez-vous répondre ?
* Quelle pourrait être la solution la plus adéquate ?
* Quel est votre marché ?
* Quels sont vos segments de clientèle ?
* Qui seront vos early adopters ?
* Quelles sont les barrières à l'entrée ?
* Comment prototyper votre produit avec zéro budget ?

# 6. Définir et suivre les bons indicateurs de sa startup, identifier et tester les bons leviers de croissance.


## Objectifs pédagogiques
* Faire face à la croissance
* Construire et suivre le tableau de bord de sa société
* Identifier et tester les bons leviers de croissance

## Déroulé
* Séquence leviers de croissance, en petits groupes de 2 ou 3, alternance de théorie et de pratique.
* Séquence pirates métriques et construire le tableau de bord de sa société, en petits groupes de 2 ou 3, alternance de théorie et de pratique.

## Des clés pour commencer à répondre aux questions
* Quels sont vos segments de clientèle ?
* Qui seront vos early adopters ?
* Quelles sont les barrières à l'entrée ?

# 7. Les outils du manager entrepreneur moderne.

## Objectifs pédagogiques
* Découvrir et tester le Host leadership
* Découvrir et tester une sélection d’outils du management 3.0
* Comprendre la différence entre leadership et management pour appliquer les bons principes au bon moment.
* Comprendre ce que sont les motivations intrinsèques et extrinsèques pour aller chercher les bons leviers de motivation dans ses équipes.

## Déroulé
* Séquence Host leadership, en petits groupes de 2 ou 3, alternance de théorie et de pratique.
* Séquence management 3.0 (delegation pocker et moving motivators), en petits groupes de 2 ou 3, alternance de théorie et de pratique.

## Des clés pour commencer à répondre aux questions
* Comment je vais faire pour appréhender sereinement la croissance de ma start-up ?

