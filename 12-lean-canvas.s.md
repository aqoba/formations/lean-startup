---
author: Thomas Clavier
title: Le lean canvas
---

#

| | |
|-|-|
| Thomas Clavier | ![](../includes/thomas-clavier.jpg){height=160px} |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) |
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>

# Le cadre

![](../includes/cadre.svg)

# Depuis la dernière fois

* Qu'avez-vous fait ?
* Qu'avez-vous appris ?

# Le Lean Canvas

![](../includes/3-steps-step-0.svg)

# Documenter la vision

Le lean canvas

# 

![](../includes/lean-canvas-fr.svg)

# Comment le remplir ?

* Remplir les cases dans l'ordre des chiffres.
* Pas plus de 15 minutes pour la première version.
* Pensez au présent : Etat des lieux à date de vos hypothèses et de votre business model.
* Laissez des sections vides si vous ne pouvez pas remplir certaines parties ou si certaines posent problème.

# Clients

* Identifier les clients et les utilisateurs cibles
* Personifiez les clients cible à travers des histoires (personas)
* Identifier les « Early Adopters »

# Problèmes

* Lister les 3 problèmes principaux
* Identifier des problèmes qui touchent personnellement le client
* Lister les alternatives

# Proposition de valeur

* Votre proposition « unique » de valeur est la caractéristique de votre produit ou de votre service qui vous rend différent sur le marché
* Partez du problème principal et dites en quoi vous, votre produit ou service est différent.
* Ciblez les Early Adopters.
* Se mettre à la place du client et imaginer ce que l'on aimerait (résultat final)

Synthèse = résultat final + délai + réponse à la première objection

> Votre pizza fraiche en 30 min ou vous êtes remboursés

# Solution

* Réfléchissez à la solution la plus simple au premier problème
* Pour la première version, il est trop tôt pour figer les solutions.  Les problèmes, les segments clients, l’UVP (Unique Value Proposition) ne sont que des hypothèses.

# Canaux

Identifier les moyens utilisés pour créer un lien avec vos clients

:::: {.columns}
::: {.column width="60%"}
* Gratuits vs Payant
* Entrant vs Sortant
* Vente directe puis vente automatisée
* Vente en direct puis vente indirecte
* Rétention puis recommandation
:::
::: {.column width="40%" class="small"}
* **Gratuit** : blog, référencement, réseaux sociaux.
* **Payant** : SEM, adwords.
* **Entrant** : blog, référencement, livre numérique.
* **Sortant** : Pub licité, Salon pro, SEM, démarchage téléphonique.
* **Rétention vs Recommanda tion** : Avoir d’abord un produit qui fasse la différence avant de faire de l’affiliation.
:::
::::

# Couts / Revenus

* Définir le prix dès le début, même pour un MVP
* Le prix fait partie du produit et définit votre segment
* Considérez les coûts et les revenus dans le cadre de la création et du lancement de votre MVP
* Break Even Point : Répondez à la question : Combien de clients sont nécessaires pour couvrir mes coûts ?

# Indicateurs clés

Identifier les indicateurs qui évaluent votre performance du moment où vous en êtes dans le projet

![](../includes/pirates-metrics.svg){height=400px}

# Avantage déloyal

* Ce qui sera difficile à copier
* Difficile à remplir, on le garde pour la fin
* La case est là pour que vous réfléchissiez à la façon dont vous pourrez vous différencier

# Pour la prochaine fois

* Préparer vos lean canvas
* Présenter un de vos Lean Canvas à un ou deux professionnels qui connaît votre segment clients/utilisateurs.

#

:::: {.columns}
::: {.column width="30%"}
:::
::: {.column width="70%"}

![](../includes/logo-aqoba.svg){height=100px}


| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) |
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |
:::
::::


<small>
Support de formation créé par Efidev, [![](../includes/logo-azae.svg){class=logo-azae-inline}](https://azae.net) et [![](../includes/logo-aqoba.svg){class=logo-aqoba-inline}](https://aqoba.net) sous licence [![](../includes/cc-by-sa.svg){class=cc-by-sa-inline}](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)
</small>

