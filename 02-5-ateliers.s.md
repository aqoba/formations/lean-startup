---
author: Thomas Clavier 
title: 5 ateliers
---

#

| | |
|-|-|
| Thomas Clavier | ![](../includes/thomas-clavier.jpg){height=160px} |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) |
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>

# Le cadre

![](../includes/cadre.svg)


# Les 5 ateliers

# Le programme

* Aujourd’hui : Introduction à la démarche Lean Startup & présentation des 5 ateliers
* Atelier 1 - Documenter la vision, pitcher son projet et planifier l’exécution
* Atelier 2 - Dérisquer son projet et mesurer la réussite
* Atelier 3 - Définir et valider sa proposition de valeur
* Atelier 4 - Définir et valider son Produit Minimum Viable
* Atelier 5 - Trouver le moteur de croissance pour valider l’adéquation produit/marché

# Atelier 1 {class="small"}

:::: {.columns class="bg-nord-6"}
::: {.column width="20%"}
![](../includes/understand.svg){height=130px}
:::
::: {.column width="80%"}
##  Comprendre

* La différence entre business plan et business model.
* Les différents types de business models.
* Comment documenter sa vision et ses apprentissages avec le Lean Canvas.
* Comprendre par quoi commencer.
* Planifier la stratégie d’exécution, le rythme et les jalons.
:::
::::

:::: {.columns class="bg-nord-5"}
::: {.column width="80%"}

## Pratiquer

* Élaboration des lean canvas du projet.
* Identifier son segment client
* Estimer le business model (revenus, structure des coûts).
* Formulation de la vision du projet.
* Production d’un support de "pitch" du projet à partir du Lean Canvas.
* Définition du macro-planning.
* Présenter et pitcher son projet

:::
::: {.column width="20%"}
![](../includes/practice.svg){height=130px}
:::
::::

:::: {.columns class="bg-nord-4"}
::: {.column width="20%"}
![](../includes/training.svg){height=130px}
:::
::: {.column width="80%"}

##  S'entrainer

* Présenter et pitcher son Lean Canvas
* Faire l'ensemble des lean canvas

:::
::::

# Atelier 2 {class="small"}

:::: {.columns class="bg-nord-6"}
::: {.column width="20%"}
![](../includes/understand.svg){height=130px}
:::
::: {.column width="80%"}
##  Comprendre

* La différence entre risques et incertitudes
* Quels sont les grands types de risques et comment les éviter ?
* Comment évaluer les risques et quels sont les méthodes pour les éviter et/ou y faire face ?
* Quels indicateurs pour mesurer sa réussite ?
* Pirate metrics et Indicateurs de projet (Qualité, Délai, Budget, Équipe)
* Comment piloter la démarche avec le Lean Dashboard ?
:::
::::

:::: {.columns class="bg-nord-5"}
::: {.column width="80%"}

## Pratiquer

* Constitution de la matrice des risques du projet.
* Évaluer des risques avec le Team Poker et priorisation.
* Écrire son backlog de gestion des risques
* Identifier des indicateurs, des objectifs.
* Expérimenter le Lean Dashboard

:::
::: {.column width="20%"}
![](../includes/practice.svg){height=130px}
:::
::::

:::: {.columns class="bg-nord-4"}
::: {.column width="20%"}
![](../includes/training.svg){height=130px}
:::
::: {.column width="80%"}

##  S'entrainer


* Mise en place du Lean Dashboard et test de son usage
* Mise à jour du Lean Canvas avec les apprentissages de la période


:::
::::
# Atelier 3 {class="small"}


:::: {.columns class="bg-nord-6"}
::: {.column width="20%"}
![](../includes/understand.svg){height=130px}
:::
::: {.column width="80%"}
##  Comprendre

* Exploration de la phase d’adéquation entre le problème et la solution
* Comment identifier le vrai problème du client cible ? Bien comprendre son besoin.
* Comment atteindre les clients cibles ?
* Comment valider ce problème qualitativement et quantitativement ?
* Comprendre les différents type de valeur
* Comment valider la solution imaginée au problème validé ?
* Comment valider l’intérêt pour la solution qualitativement et quantitativement ?

:::
::::

:::: {.columns class="bg-nord-5"}
::: {.column width="80%"}

## Pratiquer

* Définir la stratégie de validation du problème et constituer son backlog d’action
* Identifier et reconnaitre le profil de ses clients cibles avec les personas
* Préparer un entretien Client de validation du problème
* Définir la stratégie de validation de la solution et constituer son backlog d’action
* Mise à jour du Lean Canvas (Revenus) avec les hypothèses de prix du produits

:::
::: {.column width="20%"}
![](../includes/practice.svg){height=130px}
:::
::::

:::: {.columns class="bg-nord-4"}
::: {.column width="20%"}
![](../includes/training.svg){height=130px}
:::
::: {.column width="80%"}

##  S'entrainer

* Effectuer des entretiens Clients
* Valider quantitativement une hypothèse (problème, solution, prix)
* Mettre à jour le Lean Dashboard
* Mise à jour du Lean Canvas avec les apprentissages de la période
:::
::::

# Atelier 4 {class="small"}

:::: {.columns class="bg-nord-6"}
::: {.column width="20%"}
![](../includes/understand.svg){height=130px}
:::
::: {.column width="80%"}
##  Comprendre

* Comment définir le Produit minimum viable qui permettra de valider l’adéquation du produit et de son marché ?
* Comment élaborer le backlog du MVP et quels outils utiliser
* Quelle organisation mettre en place
* Comment valider la qualité du produit et la satisfaction du client à son usage ?
* Entretien client / matrice de Kano / Indicateurs d’usage / Feedback
* Comment continuer à développer la clientèle ?

:::
::::

:::: {.columns class="bg-nord-5"}
::: {.column width="80%"}

## Pratiquer

* Définir l’expérience Atelier User Story Mapping
* Choix du MVP
* Elaboration et priorisation du backlog de MVP
* Stratégie de développement du produit
* Mettre en place un management visuel pour piloter le développement du MVP (Kanban).
* Elaboration de la matrice de Kano et du questionnaire associé.

:::
::: {.column width="20%"}
![](../includes/practice.svg){height=130px}
:::
::::

:::: {.columns class="bg-nord-4"}
::: {.column width="20%"}
![](../includes/training.svg){height=130px}
:::
::: {.column width="80%"}

##  S'entrainer

* Finaliser la matrice de Kano et enquêter auprès d’au moins 20 personnes.
* Finaliser le backlog produit
* Mettre en place un Kanban pour le MVP
:::
::::

# Atelier 5 {class="small"}

:::: {.columns class="bg-nord-6"}
::: {.column width="20%"}
![](../includes/understand.svg){height=130px}
:::
::: {.column width="80%"}
##  Comprendre

* Quels sont les différents moteurs de croissance, leurs avantages et leurs inconvénients.
* Comment mesurer et analyser la croissance pour optimiser son moteur de croissance ?
* Zoom sur la notion de cohortes et le tableau de suivi
* Sur quel critère peut-on considérer que le moteur de croissance est validé ?

:::
::::

:::: {.columns class="bg-nord-5"}
::: {.column width="80%"}

## Pratiquer

* Évaluer des moteurs de croissance possibles pour les projets
* Choisir le moteur de croissance
* Définir la courbe de croissance cible pour la viabilité du business modèle et les hypothèses de jalons à atteindre
* Définir la stratégie de validation du moteur de croissance et constituer son backlog d’actions

:::
::: {.column width="20%"}
![](../includes/practice.svg){height=130px}
:::
::::

# Échanges

# 


![](../includes/logo-aqoba.svg){height=100px}


| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) |
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |


<small>
Support de formation créé par Efidev, [![](../includes/logo-azae.svg){class=logo-azae-inline}](https://azae.net) et [![](../includes/logo-aqoba.svg){class=logo-aqoba-inline}](https://aqoba.net) sous licence [![](../includes/cc-by-sa.svg){class=cc-by-sa-inline}](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)
</small>

