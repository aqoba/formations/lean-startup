---
author: Thomas Clavier
title: Entretien Solution
---

#

| | |
|-|-|
| Thomas Clavier | ![](../includes/thomas-clavier.jpg){height=160px} |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) |
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>

# Le cadre

![](../includes/cadre.svg)

# Depuis la dernière fois

* Qu'avez-vous fait ?
* Qu'avez-vous appris ?

# Entretien solution

![](../includes/3-steps-step-1.2.svg)

# Test de la solution et du pricing

* **Risque client** : Qui est intéressé par la solution au problème ? (Comment identifier les early adopters?)
* **Risque produit** : Comment aller vous résoudre le problème ? (Quelles sont les fonctionnalités minimales nécessaires au lancement de la solution ?)
* **Risque Marché** : Quel est le modèle de pricing ? (Est-ce que les clients paieraient pour la solution ? Quel prix sont-ils prêts à mettre ?)

# Définir la solution

* Identification des hypothèses
* Élaboration de la démo
* Préparation des interviews
* Réalisations des interviews
* Mise à jour du Lean canvas
* Pivots éventuels

# Critères de sortie

Être capable

* d'identifier le profil du early adopter
* d'avoir un problème important à résoudre
* de pouvoir définir l’ensemble des fonctionnalités nécessaires à la résolution du problème
* d’avoir fixé un prix que le client est prêt à payer
* de pouvoir créer une activité viable (calcul « grosse maille »)

# Vos outils

* Élément de démo
* Le tableau de bord d'apprentissage / Validation Board
* Guide d'interviews et Compte-rendus
* Lean Canvas mis à jour
* Backlog (Epics)

# Une "démo"

Une "démo" avant de développer le produit et pour définir le produit minimal viable

* La démo doit pouvoir itérer rapidement
* La démo ne doit pas prendre trop de temps à être préparée
* La démo doit être la plus réelle possible

# Entretien Solution

# Script {.small}

| | | |
|-|-|-|
|Accueillir | 2 min | Préparer le terrain |
|Recueillir les données personnelles | 2 min | Tester le segment de clientèle |
|Raconter une histoire | 2 min | Définir le contexte du problème |
|Démo | 15 min | Tester la solution |
|Tester le prix | 3min | Sources de revenus pour le problème |
|Terminer l’entretien | 2 min | la permission de le contacter de nouveau et de vous présenter à d’autres personnes |
|Consigner les résultats | 5 min | |

# Accueillir

> Objectif : Préparez le terrain

* Remerciez le client pour l'entretien
* Expliquez que vous menez des recherches sur une idée de nouveau produit
* Décrivez le process

# Données personnelles

> Objectif :
> Vous assurez que le client est bien dans la cible et identifier les caractéristiques de vos early adopters

* Depuis combien de temps existe votre socièté ?
* Taille de la société ?
* Rôle dans la société ?
* Utilisez-vous les réseaux sociaux ?
* etc.

# Raconter une histoire

* Illustrer les principaux problèmes avec une histoire
* Vérifier si le prospect se retrouve dans cette description
* Si non, enchaîner sur l’interview problème pour comprendre comment le prospect gère ces problèmes

# Démo

* Passez chaque problème en revue et illustrer la façon dont vous y répondez à l’aide la démo
* Demandez si les clients ont des questions ?
* Cherchez à savoir
    * Quels éléments parlent le plus aux clients ?
    * Quels éléments ne sont pas indispensables ?
    * Quels éléments manquent selon eux ?

# Tester le prix

* En général le prix adapté est un prix que le client accepte de payer, mais avec un peu de résistance.
* Tester le prix de lancement pour ce segment de clientèle.
* Evaluer sa réponse et ses réactions

# Le prix

:::: {.columns}
::: {.column width="50%"}
![](../includes/cristaline.jpg){height=400px}

1,99 € 
<small>le pack de 12 bouteilles de 50cl.</small>
:::
::: {.column width="50%"}
![](../includes/evian.jpg){height=400px}

1,88 € 
<small>le pack de 6 bouteilles de 50cl.</small>
:::
::::

# Terminer

* Demandez si vous pourrez les recontacter lorsque le produit ou le service seront prêts à être testés.
* Chercher un engagement, plus qu’une parole.
* Demandez qui d'autre de leur entourage vous pourriez interroger
* Remerciez pour le temps et l’aide

# Documenter

* Retranscrivez immédiatement les résultats
* Créez un modèle
* Utilisez une suite CRM

# Objectifs

> Votre solution apporte-t-elle la valeur attendue ?

* Passez en revue les résultats chaque semaine
* Ajoutez/supprimez des fonctionnalités
* Confirmez vos premières hypothèses
* Affinez le prix, envisagez d’augmenter le prix si il n’y a aucune résistance
* Cherchez à identifier les early adopter.
* Vérifiez si vous pouvez créer une activité viable avec ce prix.

# 

![](../includes/canvas-entretien-solution.svg)

# 

:::: {.columns}
::: {.column width="30%"}
:::
::: {.column width="70%"}

![](../includes/logo-aqoba.svg){height=100px}


| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) | 
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |
:::
::::


<small>
Support de formation créé par Efidev, [![](../includes/logo-azae.svg){class=logo-azae-inline}](https://azae.net) et [![](../includes/logo-aqoba.svg){class=logo-aqoba-inline}](https://aqoba.net) sous licence [![](../includes/cc-by-sa.svg){class=cc-by-sa-inline}](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)
</small>

