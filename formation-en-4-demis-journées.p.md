---
title: Lean Startup en 4 ateliers
---

## 1. Le lean canvas

* **Objectif** : présenter les fondements du lean startup, la nécéssité de rythmer et le lean canvas.
* **Déroulé** :
    * 30 min d'introduction
    * 2h00 en binôme pour faire les lean canvas (apprendre par l'exemple) On tourne toutes les 20 min. Intercaller les moments théoriques et lean canvas
    * 30 min de débrief
* **Pour la fois suivante** : préparer les Lean Canvas
* **Ressources** :
    * [Slides d'introduction Lean Startup](../modules/01-introduction.html)
    * [Slides Lean Canvas](../modules/12-lean-canvas.html)
    * [Lean canvas à imprimer](../includes/lean-canvas-fr.pdf)


## 2. Choisir le bon lean canvas et tester les hypothèses

* **Objectif** : montrer comment choisir le premier test à faire, et comment aller tester problème et cible.
* **Déroulé** : 
    * En grand groupe on prend 1 projet et on vote sur les risques
    * En petits groupe refaire l'exercice
    * Présentation du lean board
    * Phase entretien problème
    * Phase entretien cible
    * Phase entretien solution
* **Pour la fois prochaine** : revenir avec les premiers résultats des entretiens problème et cible
* **Ressources** :
    * [Choisir le bon lean canvas](../modules/21-le-bon-lean-canvas.html)
    * [L'entretien problème](../modules/25-entretien-probleme.html)
    * [L'entretien solution](../modules/32-entretient-solution.html)
    * [Canevas entretien problème](../includes/canvas-entretien-probleme.pdf)
    * [Canevas entretien solution](../includes/canvas-entretien-solution.pdf)


## 3. résumé sur les lean canvas, utiliser la value proposition canvas

* **Objectif** : Utiliser le value proposition canvas pour explorer toutes les options possible
* **Déroulé** : 
    * Présentation rapide du "value proposition canvas"
    * Atelier : 20 min pour construire la partie : profile des clients
    * théorisation sur la partie "profile client"
    * Atelier : 20 min pour construire la partie : valeur
    * théorisation sur la partie valeur
    * Atelier : comment utiliser ce canvas pour exprimer sa proposition de valeur
    * montrer le template
* **Ressources** : 
    * [Slides Matrice de Kano](../modules/42-matrice-de-kano.html)
    * [Slides value proposition canvas](../modules/31-value-proposition-canvas.html)
    * [Slides Host-Leadership](../modules/61-host-leadership.html)
    * [Value proposition canvas à imprimer](../includes/value-proposition-design-fr.pdf)
    * [Le Host-Leadership board à imprimer](../includes/host-leadership-board.pdf)


## 4. lien à faire avec les value proposition canvas pour réalisation du MVP

* **Objectif** : 
    * Parcourir les notions de MVP / MVE / earliest testable product / earliest usable product / earliest lovable product
    * Découper et planifier avec le story mapping
    * Suivre avec kanban
* **Déroulé** : 
    * Atelier de brainstorming autour des notions de MVP
    * Atelier Story mapping
    * Atelier Kanban
* **Ressources** :
    * [Slides Produit Minimum Viable](../modules/41-mvp.html)

