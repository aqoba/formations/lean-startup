REVEAL_JS_VERSION=4.4.0
REVEAL_JS_DIR=reveal.js-$(REVEAL_JS_VERSION)
FORK_AWESOME_VERSION=1.1.7
FORK_AWESOME_DIR=Fork-Awesome-$(FORK_AWESOME_VERSION)
MD_FILES = $(wildcard *.md)
SLIDES_SRC = $(wildcard *.s.md)
PARCOURS_SRC = $(wildcard *.p.md)
CLEAN=*~ *.rtf *.ps *.log *.dvi *.aux *.out *.html *.bak *.toc *.pl *.4ct *.4tc *.lg *.sxw *.tmp *.xref *.idv *.tns
SVG_FILES= $(wildcard includes/*.svg)

all: public/index.html slides parcours

debug:
	echo $(SLIDES_SRC)
	echo $(patsubst %.s.md, public/%.html, $(SLIDES_SRC))
	echo $(PARCOURS_SRC)
	echo $(MD_FILES)

reveal.js.zip:
	wget -q -O reveal.js.zip https://github.com/hakimel/reveal.js/archive/$(REVEAL_JS_VERSION).zip

fork-awesome.zip:
	wget -q -O fork-awesome.zip https://github.com/ForkAwesome/Fork-Awesome/archive/$(FORK_AWESOME_VERSION).zip

public/$(REVEAL_JS_DIR): reveal.js.zip
	unzip -o reveal.js.zip -d public

public/$(FORK_AWESOME_DIR): fork-awesome.zip
	unzip -o fork-awesome.zip -d public

public/:
	mkdir -p public/modules/
	mkdir -p public/parcours/

public/index.css: public/ index.css
	cp index.css public/

public/aqoba.css: public/ aqoba.css
	cp aqoba.css public/

public/index.html: public/ public/aqoba.css public/index.css public/$(REVEAL_JS_DIR) public/$(FORK_AWESOME_DIR) README.md
	cp -r includes public/
	(cat README.md ; ./build-modules-list.sh )  | pandoc --css=../aqoba.css --css=../index.css --standalone -f markdown+smart > public/index.html

parcours: $(patsubst %.p.md, public/parcours/%.html, $(PARCOURS_SRC))

public/parcours/%.html: %.p.md 
	pandoc --css=../aqoba.css --css=../index.css --standalone -f markdown+smart < $< > $@

public/modules/%.html: %.s.md 
	pandoc -t revealjs --slide-level=1 -s -o $@ -f markdown+smart $< -V revealjs-url=../$(REVEAL_JS_DIR)/ -V theme=white --css=../aqoba.css

public/%.pdf: %.svg
	cp $< public/includes/
	inkscape --export-type="pdf" --export-filename="$@" $< 

public/%.png: %.svg
	cp $< public/includes/
	inkscape --export-type="png" --export-filename="$@" $<

clean:
	rm -rf $(CLEAN) public

slides: $(patsubst %.s.md, public/modules/%.html, $(SLIDES_SRC)) $(patsubst %.svg, public/%.png, $(SVG_FILES)) $(patsubst %.svg, public/%.pdf, $(SVG_FILES))

ci:
	inotify-hookable $(patsubst %, -f %, $(MD_FILES)) $(patsubst %, -f %, $(SVG_FILES)) -f aqoba.css -f index.css -w includes  -c "make"

