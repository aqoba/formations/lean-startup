---
author: Thomas Clavier
title: Host Leadership
---

#

| | |
|-|-|
| Thomas Clavier | ![](../includes/thomas-clavier.jpg){height=160px} |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) |
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>

# Le cadre

![](../includes/cadre.svg)

# Depuis la dernière fois

* Qu'avez-vous fait ?
* Qu'avez-vous appris ?

# Host Leadership {data-background-image="../includes/wedding.jpg" data-state="white50"}

Un constat, tout le monde a déjà été leader, au moins sur l'organisation d'un événement, un mariage ou une soirée pizza.

* Tout le monde est capable
* Un ensemble d'automatismes qui une fois analysés peuvent nous donner les moyens d'être leader dans d'autres contextes

# 6 actions

|    |    |    |    |
|--- |--- |--- |--- |
| ![](../includes/initier.png){height=60px}    | Initier        | ![](../includes/inviter.png){height=60px}    | Inviter |
| ![](../includes/creer.png){height=60px}      | Créer l'espace | ![](../includes/garantir.png){height=60px}   | Garantir l'espace |
| ![](../includes/connecter.png){height=60px}  | Connecter      | ![](../includes/participer.png){height=60px} | Participer |

# Initier

![](../includes/initier.png){height=150px}

Écouter ce qui doit venir, avoir une vision

* Les signaux faibles.
* Les éléments qui vont permettre la mise en route.
* La vision, l'objectif, le rêve.

# Inviter

![](../includes/inviter.png){height=150px}

Faire rêver les gens pour leur donner envie de venir

* Proposer de participer à un projet, un sujet, ...
* Reconnaître la valeurs des invités
* Fournir ici des attentes, des recommandations, des informations, ...
* Donner la possibilité de refuser

# Créer l'espace

![](../includes/creer.png){height=150px}

Organiser un espace propice à l'événement, physique et émotionnel

* Un espace propice aux interactions
* Pour que chacun donne le meilleur de lui même
* En fonction de ce que l'on espère qu'il s'y passe
* Être le premier arrivé et le dernier parti

# Garantir l'espace

![](../includes/garantir.png){height=150px}

Accueillir les gens, garantir les limites et la sécurité de tous au sein de l'espace créé.

* Décider qui et quoi fera parti de l'événement, du sujet
* Fermer, ouvrir les frontières
* Mettre en place les routines, les rituels, règles de vie
* Pouvoir d'exclusion temporaire (questions, personnes, ...)
* Clore l'espace à la fin

# Connecter

![](../includes/connecter.png){height=150px}

Favoriser les rencontres, faire se rencontrer les gens.

Établir les connexions à 3 niveaux :

* l'hôte avec les invités,
* les invités entre eux,
* se connecter au sujets, aux propos.

# Participer

![](../includes/participer.png){height=150px}

Devenir un participant au même titre que tous les invités.

* Les invités d'abord
* avec les invités
* relation de parité (non dominante)
* être là au cas où (prévention, crise, etc.)

# 4 lieux
<table>
  <tr>
    <th colspan="2"><center>Derrière</center></th>
    <th colspan="2"><center>Devant</center></th>
  </tr>
  <tr>
    <td><img src="../includes/balcon.png" height=60px></td><td>Sur le balcon</td>
    <td><img src="../includes/projecteurs.png" height=60px></td><td>Sous les projecteurs</td>
  </tr>
  <tr>
    <td><img src="../includes/cuisine.png" height=60px></td><td>Dans la cuisine</td>
    <td><img src="../includes/amis.png" height=60px></td><td>Avec les invités</td>
  </tr>
</table>

Toutes ces actions peuvent être tenu dans les 4 lieux

# Sous les projecteurs

![](../includes/projecteurs.png){height=150px}

Être le centre de l'attention

* Dans quel situations ?
* Comment obtenir l'attention de tous
* Force ou faiblesse

# Avec les invités

![](../includes/amis.png){height=150px}

Être un parmi les autres

* Force ou faiblesse
* Dans quel situations ?

# Sur le balcon

![](../includes/balcon.png){height=150px}

Observer, prendre du recul

* Observer et identifier les gens qui ont besoin de connections, d'aide ou de logistique.

# Dans la cuisine

![](../includes/cuisine.png){height=150px}

Faire le travail préparatoire, mais aussi s'isoler, réfléchir, sortir du tumulte par exemple pour se recentrer sur soi.

#

[![](../includes/host-leadership-board.png){style="height: 600px"}](../includes/host-leadership-board.pdf)

# Vous avez été leader

* Quels sont vos points faibles et vos points forts ?
* Comment pouvez vous vous améliorer ?
* Quelles actions vous pouvez entreprendre pour progresser ?

# Vous serez leader

* Connaissant vos points faibles et vos points forts, comment pouvez-vous vous améliorer ?
* Quelles actions vous pouvez entreprendre pour progresser ?
* Avez-vous identifié votre cuisine ? votre balcon ?

# Vous avez observé un leader

* Quels sont ses points faibles et ses points forts ?
* Comment pouvez vous l'aider à s'améliorer ?
* Quelles actions vous pouvez entreprendre pour lui permettre de progresser ?

# Vous allez accompagner un leader

* En tant que manager, accompagner un membre de l'équipe à devenir leader.
* Faire l'état des lieux du leadership d'un collaborateur.

# Pistes d'utilisation

* Pour devenir leader
* Comme une checklist pour être un leader plus complet
* Pour se connaitre et identifier ses forces, améliorer ses faiblesses, connaitre ses positions, etc.
* Pour aider quelqu'un, avec ou sans lui.


#

:::: {.columns}
::: {.column width="30%"}
:::
::: {.column width="70%"}

![](../includes/logo-aqoba.svg){height=100px}


| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) | 
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |
:::
::::


<small>
Support de formation créé par Efidev, [![](../includes/logo-azae.svg){class=logo-azae-inline}](https://azae.net) et [![](../includes/logo-aqoba.svg){class=logo-aqoba-inline}](https://aqoba.net) sous licence [![](../includes/cc-by-sa.svg){class=cc-by-sa-inline}](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)
</small>

