---
author: Thomas Clavier
title: Pitch Deck
---

#

| | |
|-|-|
| Thomas Clavier | ![](../includes/thomas-clavier.jpg){height=160px} |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) |
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>

# Le cadre

![](../includes/cadre.svg)

# Depuis la dernière fois

* Qu'avez-vous fait ?
* Qu'avez-vous appris ?

# Pitcher sa startup

![](../includes/3-steps-step-1.svg)

# Pitch Deck

Pour pitcher sa startup, il est possible de préparer un Pitch Deck pour accompagner votre pitch oral.

Il existe plusieurs templates :

* Le deck en 10 slides
* Le deck startup week-end "Pitch fire"
* etc.

# 10 slides

* 10 concepts, le nombre optimal que votre interlocuteur peut comprendre sans perdre le fil de votre pitch
* 20 minutes, cela force à rester concentré sur le concept et à être concis. L’idée est aussi de garder un maximum de temps pour l’échange une fois la personne « hameçonnée »
* Style épuré et police de taille optimale, l’objectif est de captée l’audience.

# Introduction

Le nom de la startup, votre nom, votre fonction, le ou les moyens de vous contacter.

# Problem

Décrire le problème identifié et encore non résolu, case 2 du lean canvas.

# Solution

Décrire comment la startup apporte la solution. Quelle est la proposition de valeur ? Cases 3 et 4 du lean canvas.

# Le produit

Le but est de montrer le produit ou le service de la startup. Case 3 du lean canvas.

Expliciter la solution avec une image, une vidéo ou une démo.

# Le marché

Expliciter le marché, sa taille, bien le chiffrer. Case 1, 5 et 6 du lean canvas.

# Concurrents

Décrire les concurrents et le positionnement de la startup par rapport à eux. Comment ta startup se différencie ? Case 2, 3 et 4 du lean canvas.

# Business model

Présenter le business model. Comment la startup va gagner de l’argent ? Case 6 du lean canvas.

Présenter la stratégie de commercialisation et marketing pour atteindre les objectifs.

# Projections financières

Les chiffres clés, les milestones & les principaux accomplissements depuis la création de ta startup

# L’équipe

L’équipe et sa complémentarité. Ne pas oublier de citer les mentors et/ou les investisseurs déjà présents au capital


# Demande

* Pour une Levée de fond : le besoins financiers et la répartition optimale des fonds levés.
* Pour une demande de partenariat : ce que le partenaire va y gagner, ce qu'il va vous apporter.

> Ne pas perdre de vue qu'un pitch s'adresse à un auditoire avec des attentes ou des critères de sélection commun. Il faut donc répondre à leurs problèmes et leurs attentes avant de faire votre demande.

# Pitch fire

* 6 concepts, un style très concis, un argumentaire éclair pour faire mouche.
* 1 à 2 minutes pour convaincre de travailler ensemble.
* Style épuré et police de taille optimale, l’objectif est de captée l’audience.

# Qui ?

Présenter qui vous êtes sans étaler votre CV. C’est quelques secondes pour marquer les esprits avec votre spécialité et votre marque. Glissez une pointe d’humour dans cette première partie pour fidéliser votre auditoire.

# Problème

Vous devez raconter une histoire qui parle à tout le monde, une histoire qui illustre clairement le problème que votre projet va résoudre.

# Cible

Votre histoire illustrant le problème devrait amener tout le monde à cerner les futurs clients. Maintenant, il s’agit de définir avec précision lesquels seront les premiers utilisateurs.

# Solution

Si tout s’est bien passé, vous avez éveillé la curiosité du public, il faut donc exposer de façon simple et dynamique votre solution.

# Besoins

C’est le moment d’exprimer clairement à votre interlocuteur ce dont vous avez besoin. Dans un startup weekend, vous allez recruter une équipe de choc, face à un investisseur en capital risque, vous demanderez de l’argent.

# Conclure

Remerciez et donnez les moyens de vous contacter pour aller plus loin.

# Pour la prochaine fois

Depuis votre lean canvas en cours, préparer un "pitch deck" et testez le avec un ou deux professionnels qui connaît votre segment clients/utilisateurs.

#

:::: {.columns}
::: {.column width="30%"}
:::
::: {.column width="70%"}

![](../includes/logo-aqoba.svg){height=100px}


| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) | 
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |
:::
::::


<small>
Support de formation créé par Efidev, [![](../includes/logo-azae.svg){class=logo-azae-inline}](https://azae.net) et [![](../includes/logo-aqoba.svg){class=logo-aqoba-inline}](https://aqoba.net) sous licence [![](../includes/cc-by-sa.svg){class=cc-by-sa-inline}](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)
</small>

