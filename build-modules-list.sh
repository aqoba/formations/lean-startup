#!/bin/bash
for MODULE in *.s.md
do
    TITRE=$(grep title $MODULE | cut -d ':' -f 2)
    URL=${MODULE/.s.md/.html}
    echo "* [$TITRE](./modules/$URL)"
done

