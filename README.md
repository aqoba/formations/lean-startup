---
title: Lean Startup
---

Des modules de formation au Lean Startup regroupés en parcous de formation plus ou moins long, et donc plus ou moins complet.

## Les parcours

* Un parcours en [4 demis journées](./parcours/formation-en-4-demis-journées.html)
* Un parcours en [6 demis journées](./parcours/formation-en-6-demis-journées.html)
* Un parcours en [7 demis journées](./parcours/formation-en-7-demis-journées.html)

## Les modules
