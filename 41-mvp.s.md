---
author: Thomas Clavier
title: Définir et valider son Produit Minimum Viable (MVP)
---

#

| | |
|-|-|
| Thomas Clavier | ![](../includes/thomas-clavier.jpg){height=160px} |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) |
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>

# Le cadre

![](../includes/cadre.svg)

# Depuis la dernière fois

* Qu'avez-vous fait ?
* Qu'avez-vous appris ?

# Définir et valider son MVP

![](../includes/3-steps-step-2.svg)

# Adéquation Produit/Marché

# Développer

* Les Early adopters sont identifiés et identifiables
* Le problème essentiel est identifié
* Le prix de vente est validé
* Le modèle économique est théoriquement viable
* GET TO RELEASE 1.0 - MVP

# MVP

> We define MVP as that unique product that maximizes return on risk for both the vendor and the customer.

:::: {.columns}
::: {.column width=50%}
![](../includes/mvp-frank-robinson.png){height=300px}
:::
::: {.column width=50%}

Frank Robinson - 2001
:::
::::

# MVP

> The minimum viable product is that version of a new product which allows a team to collect the maximum amount of validated learning about customers with the least effort.

:::: {.columns}
::: {.column width=50%}
:::
::: {.column width=50%}
Éric Ries - 2009
:::
::::

# MVP

> An MVP is the minimally scoped product that gets some job done for your chosen customer archetype.

:::: {.columns}
::: {.column width=50%}
:::
::: {.column width=50%}
Steve Blank - 2010
:::
::::

# MVP

> MVP means releasing early and often, and validated learning means using metrics and A/B testing to find out what really
works and what doesn’t.

:::: {.columns}
::: {.column width=50%}
:::
::: {.column width=50%}
Henrik Kniberg - 2012
:::
::::

# MVP

> A MVP is an offer that generates revenue for the company and that motivates customers to provide feedback and
recommend it to other potential customers.

:::: {.columns}
::: {.column width=50%}
:::
::: {.column width=50%}
Robert Poole - 2012
:::
::::

# MVP

> MVP is a key concept. The goal is to identify the most valuable features by iteratively experimenting the market.

:::: {.columns}
::: {.column width=50%}
:::
::: {.column width=50%}
Yii Huumo - 2015
:::
::::

# MVP

Étude sur 92 publications qui définissent les mots MVP, bilan 18 définitions différentes.

https://www.researchgate.net/publication/301770963_MVP_Explained_A_Systematic_Mapping_Study_on_the_Definitions_of_Minimal_Viable_Product

file:///home/tom/T%C3%A9l%C3%A9chargements/MVPexplained-asystematicmappingstudyonthedefinitionsofMVP_CR.pdf

# Earliest product

![](../includes/earliest-product.png)

# MVE 

![](../includes/minimum-viable-experience.png)

# MVP / MVE / MLP

* ***MVP*** (Minimum Viable Product - Minimim Valuable Product) : l’ensemble minimum de fonctionnalités qui créent de la valeur.
* ***MVE*** (Minimum Valuable Experience) : la plus petite expérience qui permet d'apprendre quelques chose dans le but de créer le la valeur.
* ***MLP*** (Minimum Lovable Product) : l’ensemble minimum de fonctionnalités qui va permettre de fidéliser les clients.
- ***MMF*** (Minimum Marketable Feature) : La plus petite fonctionalité commercialisable
* ***MMP*** (Minimum Marketable Product) : Le plus petit produit commercialisable
* ***MMR*** (Minimum Marketable Release) : La plus petite version du produit commercialisable

# Origine

Créée par Frank Robinson en 2001, popularisée par Éric Ries et Steve Blank.

# Types de MVP

# Video d'explication

Une vidéo expliquant les nouvelles fonctionalités du produit.

# Landing Page

Une page web vantant les bénéfices du produit, invitant l'utilisateur à laisser ses coordonnés.

# Magicien d'Oz

Simuler le tout automatisé, sans savoir si c'est techniquement faisable.

# Concierge

Faire les tâches simples et rébarbatives à la main.

# Fragmentaire (Piecemeal)

Tirer profit des plateformes existantes.

# Levée de fonds Clients

Commencer par récolter des fonds et des engagements sur une plateforme de crowdfunding.

# Fonctionnalité unique

À la google à ses débuts, juste un moteur de recherche.

# Pré-commande

À rapprocher de la levée de fonds clients.

# Low tech

Le cardboard.

# Hardware

# Jugaad

Signifiant littéralement « détournement » en Tamoul, il s’agir d’une réparation ingénieuse ou d’un simple bricolage, une solution disruptive, ou le détournement d’un usage. Il est aussi synonyme de créativité : faire mieux ou aussi bien, tout en monopolisant moins de ressources.


# Minimum Viable Product

# Objectif

* Développer la solution tout en apprenant
* Mettre en place le cycle de validation qualitative
* Définir la solution minimale viable : réduire le périmètre et ne garder que le "Must Have"
* Focus sur l'apprentissage
* Développement de la solution
* Développement du client

# Activités

* Élaborer le backlog
    * Fonctionnalités incontournables identifiées
    * Le flux d'activation
    * Le site web marketing
* Développement agile de la solution minimale viable
* Cadre d’organisation recommandé : kanban
    * Déploiement continu des produits ou servives numériques (simple)

# Vos outils

* User Story Mapping
* Le Backlog
* Le questionnaire Kano
* Process projet Kanban
* La produit en Release 1.0

# User Story Mapping

* Le user story mapping a été inventé par Jeff Patton, en 2008, à Salt Lake City, Utah
* Un atelier qui permet de créer le product backlog, en travaillant sur un outil de marketing, afin de partager la vision d'un produit

# {data-background-image="../includes/story-map.jpg"}


# Épopées

* Dérouler les grands thèmes sous forme d'une histoire linéaire
* Détailler chaque thème en épopées

![](../includes/story-map-1.svg){height=400px}

# Histoires utilisateurs

* Détailler les épopées en histoires utilisateurs

![](../includes/story-map-2.svg){height=400px}

# Versions

* Regrouper les histoires utilisateurs en MVP/MVE

![](../includes/story-map-3.svg){height=400px}

# Apprentissages

Définir pour chaque version :

* l'apprentissage : 1 seul par version avec un moyen de le mesurer
* l'expérience : 1 test chiffré et limité dans le temps

![](../includes/story-map-4.svg){height=350px}

# sk8

* **Objectif** : la ou les fonctionnalités de la version skateboard
* **Apprentissage** : Valider que nos utilisateurs sont près à ....
* **Expérience** : Demander à 20 utilisateurs en 1 semaine d'utiliser la fonctionnalité et en avoir 15 qui l'utilise.
* **Planification** :
    * Itération 1 / Sprint 1
        * US
        * US ...
    * Itération 2 / Sprint 2
    * Itération 3 / Sprint 3

# La revue de backlog

* Passage en revue des prochaines itérations
* Réorganisation des sujets en fonction
    * de l’actualité
    * des opportunités
    * du niveau de préparation
    * des découvertes
    * des retours clients
* Vérification de la cohérence des sprints

# {data-background-image="../includes/revue-backlog.jpg"}

# Le management visuel

# Kanban

# Site web marketing

* Acquisition
* Unique Value Proposition (landing page)
* Autres Pages (Tour, About Us)
* Pricing
* Click Sign-up

#

:::: {.columns}
::: {.column width="30%"}
:::
::: {.column width="70%"}

![](../includes/logo-aqoba.svg){height=100px}


| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) | 
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |
:::
::::


<small>
Support de formation créé par Efidev, [![](../includes/logo-azae.svg){class=logo-azae-inline}](https://azae.net) et [![](../includes/logo-aqoba.svg){class=logo-aqoba-inline}](https://aqoba.net) sous licence [![](../includes/cc-by-sa.svg){class=cc-by-sa-inline}](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)
</small>

