---
author: Thomas Clavier 
title: Introduction au Lean Startup
---

#

| | |
|-|-|
| Thomas Clavier | ![](../includes/thomas-clavier.jpg){height=160px} |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) |
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>

# Le cadre

![](../includes/cadre.svg)

# Et vous ?

# Objectifs

Découverte de l'approche Lean Startup à travers un panorama des principes et des concepts clés, qui permettra de passer de la théorie à la pratique :

* Comprendre le lean startup, ses principes, ses concepts clés
* Comprendre les différentes étapes de la méthode "Running Lean"
* Comprendre et utiliser les principaux outils du Lean Startup : lean canvas, lean sprint, MVP, validation board, lean dash board

# Introduction au Lean Startup

* Présentation du Lean Startup
    * Pourquoi le Lean Startup
    * Les trois méta-principes
    * Les principes et les concepts clé du Lean Startup
    * Les trois phases de la démarche
    * Le rythme (Lean sprint)
* Présentation des ateliers : Objectifs, déroulé et livrables
* Échanges : Questions et vos attentes

# Qu’est-ce qu’une startup ?

# {data-background-image="../includes/bill.jpg"}

#

> Une startup est une structure commerciale organisée par des personnes qui cherchent à concevoir un nouveau produit ou service dans des conditions d’extrême incertitude.

**Eric Ries**

# 

> Une startup est un projet qui a pour objectif la recherche d’un modèle d’affaires duplicable et surdimensionnable

**Steve Blank**

# Le Lean Startup

# Le Lean Startup

Mouvement qui transforme le developpement et le lancement des nouveaux produits.

Une philosophie entrepreneuriale qui se base sur l'application du lean management à l'entreprenariat.

Un guide pour le pilotage d'une startup, le developpement des produits et des clients.

# Pourquoi le Lean Startup ?

# Pourquoi

* La plupart des startup échouent encore.
    * 9 startups sur 10 échouent.[^1]
* Répondre aux problématiques inédites des startup et de l’innovation
* Apporter davantage de rigueur à l’entreprenariat et à l’innovation.
* Fournir les outils nécessaires et proposer un process de création d’une activité innovante.

[^1]: source http://www.statisticbrain.com/startup-failure-by-industry et http://fortune.com/2014/09/25/why-startups-fail-according-to-their-founders/

# Échec ?

:::: {.columns}
::: {.column width="50%"}
* Pas d’adéquation Produit/Marché
    * Le produit est génial mais n’a pas de client
* Trop de planification
    * Business Plan
    * méthodologies de gestion traditionnelles des entreprises matures
* « Just do it »
    * éviter toute forme de gestion amène au chaos.
:::
::: {.column width="50%"}
![](../includes/echec.jpg)
:::
::::

# Les fondateurs

# Customer Development

:::: {.columns}
::: {.column width="60%"}
* Steve Blank, entrepreneur, professeur à l’Université de Stanford, l’U.C. Berkeley et à Columbia
* Créateur du développement de la clientèle (customer development)
* [steveblank.com](http://steveblank.com)
:::
::: {.column width="40%"}
![](../includes/book-steve-blank-1.jpg){height=220px}
![](../includes/book-steve-blank-2.jpg){height=220px}
:::
::::

#

> Aucun business plan ne survit au 1er contact avec le client !

**Steve Blank**

# Business Model Generation

* Alexander Osterwalder, consultant
* Yves Pigneur, professeur de management et de gestion des systèmes d'information à l'université de Lausanne

Créateurs du Business Model Generation, du Business Model canvas et du Value Proposition Canvas

[businessmodelgeneration.com](http://businessmodelgeneration.com)

![](../includes/book-business-model.jpg){height="3em"}
![](../includes/business-model2.jpg){height="3em"}
![](../includes/business-model3.jpg){height="3em"}
![](../includes/business-model4.jpg){height="3em"}

#

> La principale limite du business plan vient du fait qu’il est souvent réalisé par des entrepreneurs au début de leur phase exploratoire, en fonction d’hypothèses et de chiffres non-vérifiés, alors qu’il devrait être rédigé une fois que le business model de l’entreprise a été testé, vérifié et amélioré.

**Alexandre Osterwälder**

# Lean Startup

:::: {.columns}
::: {.column width="60%"}
* Eric Ries
* Entrepreneur (IMVU)
* Auteur du livre Lean Startup
* [www.startuplessonslearned.com](http://www.startuplessonslearned.com)

![](../includes/book-lean-startup.jpg){height=250px}
![](../includes/book-the-leader-s-guide.jpg){height=250px}
:::
::: {.column width="40%"}
![](../includes/eric-ries.jpg)
:::
::::

#

> La passion, l’énergie et la vision que les entrepreneurs investissent dans leurs projets sont des ressources trop précieuses pour être gaspillées

**Eric Ries**

# Running Lean

:::: {.columns}
::: {.column width="60%"}
* Ash Maurya, entrepreneur
* [practicetrumpstheory.com](https://practicetrumpstheory.com)
* [www.leanstack.com](http://www.leanstack.com)

![](../includes/book-running-lean.jpg){height="5em"}
![](../includes/book-scaling-lean.jpg){height="5em"}
![](../includes/lean-canvas-fr.svg){height="5em"}
:::
::: {.column width="40%"}
![](../includes/ash-maurya.jpg)
:::
::::

#

> La vie est trop courte pour concevoir un produit dont personne ne veut.

**Ash Maurya**

# Métriques

:::: {.columns}
::: {.column width="60%"}
* Dave McClure, entrepreneur
* Fondateur de l'accélérateur 500 Startups
* [500hats.com](http://500hats.com/)
* Inventeur des Startup Metrics for Pirates.
:::
::: {.column width="40%"}
![](../includes/pirates-metrics.svg)
:::
::::

#

> Les clients ne s’intéressent pas à vos solutions, ils se soucient de leurs problèmes

**Dave McClure**

# PDCA

:::: {.columns}
::: {.column width="60%"}
W. Edwards Deming, statisticien, professeur, auteur et consultant américain.

![](../includes/pdca.jpg)
:::
::: {.column width="40%"}
![](../includes/deming.jpg)
:::
::::

# 

> You can’t manage what you don’t measure.

**W. Edwards Deming**

# Les origines du Lean Startup

# Lean management

Le lean est une méthode de management qui vise l’amélioration des performances de l’entreprise par le développement de tous les employés. La méthode permet de rechercher les conditions idéales de fonctionnement en faisant travailler ensemble personnel, équipements et sites de manière à ajouter de la valeur avec le moins de gaspillage possible.

Le double objectif du Lean management est la satisfaction complète des clients de l’entreprise et l'épanouissement de chacun des employés. 

# 4 principes Lean

* Comprendre ce qui plait au client pour spécifier la valeur du service ou du produit
* Augmenter le niveau de Juste-à-temps, c’est à dire réduire le délai entre la commande client et la livraison du produit ou de l’offre
* S’arrêter à chaque défaut et résoudre le problème plutôt que le contourner
* Impliquer les opérateurs dans l’amélioration et la reconception de leurs environnements de travail

# Agilité

L'agilité est une culture qui vise l'amélioration des performances de l'entreprise par la satisfaction client, le développement des employés et l'amélioration de la collaboration.

> Le Lean Startup s’inspire et utilise les modèles de développement agile de produits


# 4 valeurs de l'agilité

* Individus et interactions plus que processus et outils.
* Produit fonctionnel plus que documentation exhaustive.
* Collaboration avec le client plus que négociation du contrat.
* Adaptation au changement plus que suivi d'un plan.

# Le développement de la clientèle

![](../includes/produit-marche.svg){height=300px}

Adéquation Produit/Marché

# Référence

![](../includes/reference.svg)


# Le Lean Startup

# L’objectif

> Le plus grand risque pour une startup est de développer quelque chose dont personne ne veut.

L'objectif est donc de dé-risquer le projet en développant parallèlement le produit et les clients de façon incrémentale et itérative en capitalisant sur les apprentissages, avant de monter en puissance.

# 5 principes

# 1 - Les entrepreneurs sont partout

* Est Entrepreneur quiconque dirige une startup définie par une structure commerciale destinée à concevoir de nouveaux produits ou services dans des conditions d’extrêmes incertitudes.
* Fondateur, Intrapreneur, Chef d’entreprise innovante, ...
* Du garage au grand groupe.

# 2 - L’entreprenariat est une forme de management

* La startup est une institution pas seulement un produit. Elle requiert un nouveau management guidé par la vision et spécialement adapté au contexte d’incertitude.
* Le créateur de startup doit recourir à une discipline managériale.
* Les risques doivent être éliminés de manière méthodique.

# 3 - La validation des enseignements

* Une startup a pour objectif d’apprendre comment créer une activité viable à long terme
* Le vrai produit de la startup est son modèle économique
* Elle doit confirmer ces enseignements et doit recourir régulièrement à des expérimentations menées avec une rigueur scientifique pour tester chaque élément de sa vision

# 4 - La boucle de feedback

![](../includes/feedback.svg){height=400px}

# La conduite de projet par cycles

Kaizen / PDCA - Approche scientifique - Scrum

![](../includes/cycle-projet.svg){height=300px}

# 5 - La gestion analytique de l’innovation

* Définir, mesurer et communiquer les progrès
* Définir les indicateurs décisionnels
* Atteindre les objectifs en réglant le moteur de croissance
    * fidélité
    * viralité > 1
    * acquisition payante
* Pivoter ou persévérer

# Les concepts

# La vision

* Le but, la destination de la startup.
* Créer une activité prospère qui changera la face du monde
* Donne en permanence le cap

# L’innovation

* **Innovation de l’offre** (le quoi) : le développement de nouveaux produits ou de nouveaux services qui n’existaient pas avant
* **Innovation de processus** (le comment) : le développement de nouveaux procédés ou méthodes qui permettent de livrer le produit ou service plus vite, moins cher, et de meilleure qualité.
* **innovation de marché** (le qui) : viser un marché présentement mal servi par les offres actuelles des autres, en faisant des modifications mineures à son produit ou à son service.
* **innovation de valeur** (le pourquoi) : viser un besoin non-satisfait mais très apprécié de son marché actuel, et livrer cette valeur de façon plus efficace que ses compétiteurs, en intégrant l’innovation de processus.

# Le modèle d’affaire

* Le business model représente comment une entreprise gagne de l’argent.
* C’est le « vrai produit » de la startup
* C'est le poumon de l'entreprise

# Hypothèse

* Tout ce qui doit être validé ou invalidé
* Elle doit être réfutable et vérifiable
* Elle doit être validée qualitativement et quantitativement

# Le MVP <small>Minimum Viable Product</small>

> Le Minimum Viable Product est la version du nouveau produit qui permet à l’équipe de collecter le maximum d’enseignements validés auprès des clients avec le moindre effort

**Eric Ries**

# Le MVE <small>Minimum Viable Experience</small>

La plus petite expérience que l'on peut faire pour apprendre quelque chose sur son modèle d'affaire.


# Pivot

> Le pivot est un changement substantiel d’une ou plusieurs composantes du business model.

**Steve Blank**

# Différents type

* Zoom-in Pivot
* Zoom-out Pivot
* Customer Segment Pivot
* Customer Need Pivot
* Technology Pivot
* Channel Pivot
* Plateforme Pivot, Business Architecture pivot, Value capture pivot, growth engine pivot

# Indicateurs

* Les indicateurs doivent être définis pour mesurer l’apprentissage et la progression
* Attention aux indicateurs de vanité
* Les indicateurs suivent la règle des trois « A »
    * **Actionable** : Facile à mettre en place,
    * **Accessible** : Facilement compréhensible et qui peut être obtenu rapidement
    * **Auditable** : Vérifiable

SMART - Significatif; Mesurable; Actionnable; Temporellement défini.


# Genchi Genbutsu (go and see)

* Un des principes clés du Lean
* « Sortir du bureau » - Steve blank


  * Observer par soi-même
  * Aller à la rencontre des clients et des utilisateurs
  * Privilégier les interactions avec les personnes

# Le process

Le Lean Startup propose de suivre un process lean s'appuyant sur trois meta-principes permanents

* Documenter la vision pour capitaliser l'apprentissage
* Identifier les éléments les plus risqués de cette vision
* Systématiquement tester la vision pour valider les apprentissages

#

> La réussite d’une startup peut être conçue en suivant le processus, ce qui signifie qu'il peut être appris, ce qui signifie qu'il peut être enseigné

**Eric Ries**

# les 3 étapes d’une startup

![](../includes/3-steps.svg)

# Running Lean : Lean Startup en pratique

# Rappel

:::: {.columns}
::: {.column width="60%"}
Les trois meta-principes du Lean Startup

* Documenter les hypothèses pour capitaliser l'apprentissage
* Identifier les éléments les plus risqués de cette vision
* Systématiquement tester chaque hypothèse pour valider les apprentissages
:::
::: {.column width="40%"}
![](../includes/feedback-simple.svg)
:::
::::

# 3 étapes d’une startup

Ses principes s'appliquent dans les trois phases du process

![](../includes/3-steps-split.svg)

# Préparer la croissance

À l'issue des phases 1 et 2, l'objectif est d'avoir

* Un produit minimum viable
* Un entonnoir de conversion qui fonctionne
* Un modèle économique viable défini
* Une satisfaction client exprimée : 40% des early adopters qui sont "accrocs" aux produits

C’est l'apprentissage qui permet de passer à la phase croissance

# Lean Sprint

Le succès d’une démarche Lean Startup se joue sur l'adoption de
pratiques mais aussi sur la tenue du rythme des boucles
d’apprentissage.

# Lean Sprint

La méthode Running Lean introduit pour cela la notion de Sprint Lean

* itérations de 1 à 2 semaines sur un principe proche du sprint du framework agile Scrum,
* avec des cérémonies de planification, de points quotidiens de coordination et de revue de sprint.
* La durée fixe (time box) est utile pour encourager la prise de décision

# 

![](../includes/planning.png)

# Déroulé d'un Sprint

1. **Soumettre les problèmes** : Identifier la contrainte, l’hypothèse ?.
2. **Définir les solutions** : Déterminer comment lever cette contrainte, vérifier l’hypothèse?
3. **Lister les solutions** : Sélectionner les meilleures stratégies.
4. **Tester les solutions** : Tester ces stratégies avec les expériences.
5. **Décider des solutions** : Décider des actions à venir.


# Échanges

# 


![](../includes/logo-aqoba.svg){height=100px}


| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) |
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |


<small>
Support de formation créé par Efidev, [![](../includes/logo-azae.svg){class=logo-azae-inline}](https://azae.net) et [![](../includes/logo-aqoba.svg){class=logo-aqoba-inline}](https://aqoba.net) sous licence [![](../includes/cc-by-sa.svg){class=cc-by-sa-inline}](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)
</small>

