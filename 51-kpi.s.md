---
author: Thomas Clavier
title: Trouver son moteur de croissance
---

#

| | |
|-|-|
| Thomas Clavier | ![](../includes/thomas-clavier.jpg){height=160px} |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) |
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>

# Le cadre

![](../includes/cadre.svg)

# Depuis la dernière fois

* Qu'avez-vous fait ?
* Qu'avez-vous appris ?

# Market fit

![](../includes/3-steps-step-2.2.svg)

# Prérequis

* Le MVP est établi et validé qualitativement
* Il s’agit alors de valider toute initiative qui vise à améliorer un élément clé du produit/service, pour identifier et mettre au point le moteur de croissance.
* L’analyse du comportement du client cible est primordiale.
* Mais régler le moteur suppose que les hypothèses soient clairement définies et suffisamment précises pour permettre une expérimentation de qualité.

# Identifier vos moteurs de croissance

* Il existe 3 principaux moteurs de croissance.
    * Moteur sur la fidélisation
    * Moteur sur la viralité
    * Moteur sur l’acquisition
* Inscrivez-vous au début dans un seul de ces moteurs.
* Combiner plusieurs moteurs à la fois est possible mais risque de rendre les interactions entre ces différents modèles plus difficiles et entrainer la confusion dans l’analyse des apprentissages.

# Moteur sur la fidélisation

* L’objectif : Taux élevé de rétention
* L’indicateur clé : Taux d’acquisition / Taux d’attrition (résiliation)
* C’est un moteur qui convient particulièrement bien aux places de marché, aux sites d’annonces, aux plates-formes de streaming, aux éditeurs de jeux.
* Elle repose souvent sur un principe de relance clients par la proposition de nouveau produits

# Exemples

* Des jeux « casuals » : Hidden Object, Time Management, Match 3, Casino, Adventure, Puzzle, Family, Arcade & Action, Card & Board, Strategy, Mahjong, Brain Teaser, Marble Popper, Word
* Une communauté (blog, facebook, twitter, Pinterest, Youtube) 
* Un site marchand

# Moteur sur la viralité

* L’objectif : On cherche un coefficient de viralité > 1.
* Un coefficient de 0,1 signifie que 1 client sur 10 rapportera un nouveau client.  On considère qu’un coefficient d’une valeur de 1 est en équilibre (1 sur 1 rapporte 1 nouveau client).
* L’indicateur clé : Nb de clients rapportés par un client déjà acquis.
* C’est un moteur qui convient particulièrement aux services cloud (dropbox, trello), aux jeux vidéos (jeux sociaux), aux services en lignes, (parrainage), aux réseaux sociaux. Il s’appuie d’ailleurs souvent sur les réseaux sociaux.

# Exemple

Fortnite Battle Royale.
Fortnite bénéficie également de sa viralité sur les réseaux sociaux. Le joueur est régulièrement amené à faire des choses drôles ou intenses: Danses, costumes délirants, moments de tension se partagent bien sur les réseaux sociaux.

# Moteur par acquisition payante

* L’objectif : Coût d’acquisition client (COCA) < Profit généré par un client Life Time Value (LTV) -- **LTV = 3 x COCA**
* L’indicateur clé : Coût d’acquisition client (COCA)
    * COCA = total de investissements en acquisition client/nombre de clients acquis
    * On se sert d’un partie des revenus pour l’acquisition

::: {class=small}
* Quand le coût d’acquisition d’un client est plus bas que le profit généré par ce client ; on parle alors de profit marginal et le moteur est en croissance.
* C’est un moteur qui est largement utilisé par tous types d’entreprise
:::

# Leviers d’acquisition

* Le **content marketing** : Produire des contenus de qualité afin d’attirer des prospects et les transformer en clients.
* Le **référencement naturel** (SEO) : optimiser la visibilité d’un site Web dans les pages de résultats des moteurs de recherche
* Le **display** : C’est une variante de la publicité digitale. ou l’annonceur paye pour afficher des annonces sur des sites Web de même thématique que celle de l’annonceur.

# Leviers d’acquisition

* Le **retargeting** : Il consiste à montrer un message publicitaire aux personnes qui ont été exposées au moins une fois à votre marque.
* Les **réseaux sociaux** : Ils sont incontournables pour soigner son image sur ces réseaux sociaux et créer une communauté.

# Leviers d’acquisition

* La publicité sur les médias classiques
* Les salons professionnels
* La prospection téléphonique

# Vendre votre produit

> Comment développer la visibilité et la notoriété de votre produit. ?


* Il faut faire parler de votre produit.
* Pour le faire connaître, il faut être relayé par des influenceurs, des leaders d’opinions. Ce sont ceux qui font la pluie et le beau temps sur votre marché.

# Les leaders d’opinions

* **Les identifier** : Ils sont blogueurs, journalistes ou experts dans leur domaine. Ils écrivent sur le web, parlent sur les médias et sont suivis par une communauté importante.
* **Interagir avec eux** : pour qu’ils vous reconnaissent. Pour cela, il faut : les suivre sur les réseaux sociaux, relayer leur publication, commenter leurs articles de façon pertinente, les rencontrer dans le monde physique.
* **Les solliciter** : Par exemple en obtenant leur email personnel et en les contactant pour qu’ils essaient votre produit pour en parler ensuite.

# La presse

Vous pouvez aussi contacter ou soumettre des articles sur les sites tel que Les Pépites Tech, techcrunch, thenextweb, infoQ, Maddyness, Product Hunt, Gamasutra,... sans oublier la presse spécialisée ou locale.

# Comment mesurer ?

* Activités
    * Définir les métriques
        * Utiliser les Pirats Metrics : Acquisition - Activation - Rétention - Revenu - Référencement
    * Mettre en place la plate-forme analytics (développement?)
    * Mettre en place et produire le Tableau de bord de conversion

* Vos outils
    * Développement/Intégration de la plate-forme analytics par cohortes
    * Tableau de bord de conversion

# Mesurer

**Actionable metrics** vs **Vanity Metrics**

# Pirates Metrics

![](../includes/pirates-metrics.svg)

# Aquisition

:::: {.columns}
::: {.column width="30%"}
![](../includes/pirates-metrics-aquisition.svg)
:::
::: {.column width="60%"}
Comment les clients vous trouvent et s'inscrivent ?

**Taux d’acquisition** : rapport entre le nombre d’arrivées sur le service ou sur le lieu de la communication et le démarrage d’une interaction.

On mesure la capacité du message à engager le visiteur
:::
::::

# Activation

:::: {.columns}
::: {.column width="30%"}
![](../includes/pirates-metrics-activation.svg)
:::
::: {.column width="60%"}
Comment se passe leurs premières expériences ?

**Taux d’activation** : rapport entre le nombre d’arrivées sur le service ou sur le lieu de la communication et une utilisation "réussie". 

On mesure la qualité du système, i.e. l’adéquation entre la solution et la proposition de valeur initiale.
:::
::::

# Rétention

:::: {.columns}
::: {.column width="30%"}
![](../includes/pirates-metrics-retention.svg)
:::
::: {.column width="60%"}
Est-ce que les clients reviennet ?

* **CLV (Customer Lifetime Value)** : rapport entre le revenu total reçu par un client par la durée de vie du client.
* Nombre d’achats effectués par mois / client 
* Récurrence d’achat / client

On mesure la capacité du système à faire revenir le visiteur ou à le garder sur place avec d'autres produits ou services.
:::
::::

# Recommendation

:::: {.columns}
::: {.column width="30%"}
![](../includes/pirates-metrics-reference.svg)
:::
::: {.column width="60%"}
Les clients en parlent-ils autour d'eux ?

La recommandation est l’ensemble des actions entreprises par vos utilisateurs actuels
pour parler de vous.

Attention : les "inﬂuenceurs" ne sont pas forcément les meilleurs acheteurs.

On mesure la capacité du système à s'auto-alimenter.
:::
::::

# Revenus

:::: {.columns}
::: {.column width="30%"}
![](../includes/pirates-metrics-revenus.svg)
:::
::: {.column width="60%"}
Comment les revenus sont générés ?

On mesure la capacité du système à produire de la valeur. Pas forcément financière.
:::
::::

# 3 règles pour mesurer

* Choisir la bonne mesure, le bon indicateur
* Créer des rapports simples
* Derrière les indicateurs se cachent des gens

# Choisir les bonnes données

Pour valider l’adéquation marché / produit de façon qualitative et quantitative, quelles questions se poser ?

* Comment les clients vous trouvent et s’inscrivent ?
* Comment se passe leurs premières expérience ?
* Est-ce que les clients reviennent ?
* Comment les revenus sont générés ?
* Les clients en parlent-ils autour d’eux ?

# Atelier

* Mesure des actions d’Activation.
* Mesure des actions de Rétention

# Créer des rapports simples

* Simple, Visuel, Cartographie le flux d’activation
* ex : Entonnoir de conversion

# 
Les entonnoirs seuls ne suffisent pas,
Utilisez les cohortes !

# Mesurer la conversion

![](../includes/tableau-conversion.png)

# Derrière se cachent des gens

Collectez les moyens de les contacter en cas d’échec comme en cas de succes.

# Les outils

* Google Analytics
* Kissmetrics
* Matomo.org
* Mixpanel
* keen.io
* etc.

#

![](../includes/pirate-metrics-canvas.svg)

# Valider quantitativement

![](../includes/3-steps-step-2.2.svg)

# Vérifier quantitativement

Objectif : Vérifier la capacité à montée en puissance

* Avoir un taux de rétention de 40%
* Passer le test de Sean Ellis

# Growth hacking

Le mot apparaît en 2010, créé par Sean Ellis, le fondateur de growthhackers.com. Le growth hacker ou pirate de croissance tente d’optimiser le tunnel de conversion en optimisant chaque étape de ce cycle par des expériences permettant de faire grandir l'entreprise

# Test de Sean Ellis

La question clé : "Comment vous sentiriez-vous si vous ne pouviez plus utiliser ce produit ?"

* Très déçu
* Un peu déçu
* Pas déçu
* Je n’utilise plus ce produit

> Si 40% des utilisateurs se déclarent Très déçus, vous avez toutes les chances d'acquérir durablement de la clientèle avec ce produit

# Vérifier quantitativement

Activité

* Contraindre les fonctionnalités et prioriser le backlog ( KANO)
* Mesurer les progressions (Tableau de conversion, cohortes)
* Émettre et tester les hypothèses (Backlog)
* Analyser les métriques
* Passer le test de Sean Ellis

# Vérifier quantitativement
Vos outils

* Dashboard des apprentissages et Lean Canvas mis à jour
* Tableau de conversion
* Backlog mis à jour
* La plate-forme en realease 1.x

# Croissance

![](../includes/3-steps-step-3.svg)

# Optimiser vos moteurs de croissance

#

* Le pivot ultime est la transition vers l’ouverture au grand public.
* Les deux premières phases de la startup d’adressaient à vos "early-adopters".
* L’objectif de ce pivot est de passer le cap pour devenir une entreprise viable et rentable et pérenne.


# Obtenir de la croissance et optimiser

* Vous avez atteint "l’adéquation produit / marché"
* Un certain niveau de succès est garanti
* Vous pouvez vous concentrer sur la croissance
* Continuez à régler le moteur de croissance pour "franchir le gouffre" entre vos early adopters et votre cœur de clientèle.

#

![](../includes/croissance.jpg)

# Préparer à la création de l'Entreprise

Le projet entre alors dans un second cycle de vie.

* Documenter le business model de l'entreprise
* Établir le business plan
* Disposer des éléments pour préparer le premier tour de table.
* Préparer la structure de l'entreprise pour une poursuite de l'activité à grande échelle.

# Préparer à la création de l'Entreprise

Vos outils : 

* Business model generation canvas

# 

![](../includes/business-model-canvas.png)

# Tous mes vœux de réussite pour vos projets

#

:::: {.columns}
::: {.column width="30%"}
:::
::: {.column width="70%"}

![](../includes/logo-aqoba.svg){height=100px}


| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) | 
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |
:::
::::


<small>
Support de formation créé par Efidev, [![](../includes/logo-azae.svg){class=logo-azae-inline}](https://azae.net) et [![](../includes/logo-aqoba.svg){class=logo-aqoba-inline}](https://aqoba.net) sous licence [![](../includes/cc-by-sa.svg){class=cc-by-sa-inline}](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)
</small>

